import { serverAddress } from 'constants.ts';
import { pages } from 'renderEngine.ts';

function httpGet(address, body='') {
    return new Promise(function(resolve, reject) {
        ((body==='') ? 
        (fetch(address)):(fetch(address, { body: JSON.stringify(body) })))
        .then(function(response) {
            return response.json();
        }).then(function(json) {
            let json = JSON.stringify(json);
            resolve(json);
        }).catch(function(err) {
            reject(err);
        });
    });
}

function pingServers() {
    return new Promise(function(resolve, reject) {
        httpGet(serverAddress+'/ping.js').then(function(res) {
            (res.status === 'online') ? resolve(true):reject(res.status);
        }).catch(function(err){
            reject(err);
        });
    });
}

function loadLoginPage() {
    pages.join();
}

loadLoginPage();