const app = document.getElementById('app');

export const pages = {
    join: function(name='') {
        // Set app css
        app.classList.add('red-bg');

        // Title
        let titleContainer = document.createElement('div');
        titleContainer.classList.add('title-container');
        // Create titles
        let title = document.createElement('h1');
        title.classList.add('green-txt');
        title.innerHTML = 'Apples2Apples';
        titleContainer.appendChild(title);
        let subtitle = document.createElement('h3');
        subtitle.classList.add('darker-green-txt');
        subtitle.innerHTML = 'Something funny here';
        titleContainer.appendChild(subtitle);

        // Append all the children
        app.appendChild(titleContainer); //     Title
    }
};