# Apples2Apples

Apples to Apples game but online

## The plan

### Client

Hosted at filipkin.com
- Login page
    - Connect (first 6 digits of id)
    - Name (max 32 characters, no bad words)
- Game page
    - Scoreboard sidebar
        - Player name
        - Rounds won
    - Round panel
        - Judge name
        - Icons to represent turned in cards
    - Card panel
        - List your cards during normal time
        - When picking time starts
            - Slide cards in from top
            - Flip cards over
            - Darken and blur card panel
        - When picking time ends
            - Selected card glows
            - Cards reveal who turned them in with their names above the card
            - Wait x amount of time
            - Slide cards down out of view
            - Undarken and unblur card panel
- Game end screen
    - Scoreboard
        - Placing (1st, 2nd, ect.)  
        - Player name
        - Rounds won
        - Card combinations that were selected

### Server

Hosted at nas.filipkin.com
- Game Object
    - cards
        - id (uuid v4)
            - word
            - synonyms
            - usage
            - defenition
    - games
        - id (uuid v4)
            - started
            - players
                - id (uuid v4)
                    - name
                    - score
                    - won matches
                    - cards
                    - socket
            - usedCards
                - <card object>
            - state
                - roundState (submit/select/review)
                - roundJudge (player object)
- Files
    - CardsDB
    - Constants
        - reviewTime
        - idSubStr
- HTTP server
    - Static files
- WebSocket
    - idk yet, lets find how this plays out.